
<p align="center">
<img src="assets/images/logo.png" height="200px" alt="Moon Lang" title="Moon Lang">
</p>

<b>Version</b> <small>1.0.0</small>

> An awesome, super simple language!

* Simple and lightweight
* No external dependencies
* Straight recursive descent paser

[GitHub](https://github.com/crazywolf132/moonlang)
[Lets go!](#Moon-Lang)

